tabContraintes = [];
tabValeurs = [];

window.onload = () => {
    let inputNom = document.getElementById("filtreNom");
    inputNom.value = "";
	listeInstruments = getListeInstrumentsDefaut();
	afficherListe(listeInstruments);
}

function ajouterCritere(critere, valeur) {
	let index = tabContraintes.indexOf(critere);
	if (index > -1) {
		tabContraintes[index] = critere;
		tabValeurs[index] = valeur;
	} else {
		tabContraintes.push(critere);
		tabValeurs.push(valeur);
	}
}

function retirerCritere(critere) {
	let index = tabContraintes.indexOf(critere);
	if (index > -1) {
		tabContraintes.splice(index, 1);
		tabValeurs.splice(index, 1);
	}
}

function getListeInstruments() {
	if(tabContraintes){
		return getInstrumentsSelonCriteres(tabContraintes, tabValeurs);
	}
	else {
		return getTousInstruments();	
	}
}

function getListeInstrumentsDefaut() {
	tabContraintes = [];
	tabValeurs = [];
	if(document.location.search) { //si on a fait une recherche
		let recherche = document.location.search; //va chercher la methode GET
		recherche = recherche.split('?')[1]; //enleve le ? au début
		recherche = recherche.split('&'); //type=frappe, couleur=rouge ...
		//let tab = {};
		for (let critere in recherche) {
			let tabContrainte = recherche[critere].split('=');
			ajouterCritere(tabContrainte[0], tabContrainte[1]);
		}
		return getInstrumentsSelonCriteres(tabContraintes, tabValeurs);
	}
	else {
		return getTousInstruments();
	}
}

function afficherDepart() {
	listeInstruments = getListeInstrumentsDefaut();
	afficherListe(listeInstruments);
}

function afficherTout() {
    listeInstruments = getTousInstruments();
	afficherListe(listeInstruments);
}

//à modifier pour prendre une liste "neuve" à chaque fois
function clicFiltreNom() {
    let nom = document.getElementById("filtreNom").value;
	
	ajouterCritere("nom", nom);

	listeInstruments = getInstrumentsSelonCriteres(tabContraintes, tabValeurs);
    afficherListe(listeInstruments);
}

function nomOnChange() {
	clicFiltreNom();
}

function afficherListe(listeInstruments) {
    
	let divListe = document.getElementById("liste-instruments");
    divListe.innerHTML = "";

    for (let instrument in listeInstruments)
	{
		let para = document.createElement("div");
		let node = document.createTextNode(listeInstruments[instrument][0] + ' : ' + 
		listeInstruments[instrument][1].type + ' : ' +
		listeInstruments[instrument][1].origine);
        para.className = "col-xs-1 col-sm-4 col-md-3 col-lg-5 instrument";

        let image = document.createElement("img");
        image.src = listeInstruments[instrument][1].SrcImage1;
        image.setAttribute("alt", "erreur de chargement image");
        image.className = "img-fluid";

        para.appendChild(image);
		para.appendChild(node);

		divListe.appendChild(para);
	}
}

function clicTous() {
	retirerCritere("continent");
	listeInstruments = getInstrumentsSelonCriteres(tabContraintes, tabValeurs);
    afficherListe(listeInstruments);
}

function clicAmerique() {
	filtreContinentOrigine("Amerique");
}
function clicEurope() {
	filtreContinentOrigine("Europe");
}
function clicAfrique() {
	filtreContinentOrigine("Afrique");
}
function clicAsie() {
	filtreContinentOrigine("Asie");
}

function filtreContinentOrigine(continent) {
	ajouterCritere("continent", continent);
    listeInstruments = getInstrumentsSelonCriteres(tabContraintes, tabValeurs);
    afficherListe(listeInstruments);
}

function clicReinitialiser() {
    let inputNom = document.getElementById("filtreNom");
    inputNom.value = "";
    afficherDepart();
}
