﻿//retourne tous les instruments
function getTousInstruments()
{
    let liste = [];
    let listeInstruments = instruments[0];
    for (let instrument in listeInstruments)
    {
        liste.push([instrument, listeInstruments[instrument]]);
    }
    return liste;
}

//reçoit un tableau de critères et un tableau de valeurs
function getInstrumentsSelonCriteres(criteres, valeurs)
{
    let listeInstruments = instruments[0];
    let liste = [];
    let valide = true;
    //pour chaque instrument dans le json
    for (let instrument in listeInstruments)
    {
        valide = true;
        //pour chaque critère donnés
        for (let i = 0; i < criteres.length; i++)
        {
            //cas spécial si on cherche un nom
            if(criteres[i] == "nom")
            {
                //si le critère n'est pas une sous-chaîne de caractère du nom de l'instrument, ne l'ajoute pas à la liste
                if(!instrument.includes(valeurs[i]))
                {
                    valide = false;
                }
            }
            else
            {
                //si le critère ne correspond pas, n'ajoute pas l'instrument à la liste
                if(valeurs[i] != listeInstruments[instrument][criteres[i]])
                {
                    valide = false;
                }
            }
        }
        if(valide)
        {
            liste.push([instrument, listeInstruments[instrument]]);
        }
    }
    return liste;
}
